<?php
define('PFAD_ROOT', '/var/www/vhosts/20up.io/gve/');
define('URL_SHOP', 'https://gve.twentyup.de');
define('DB_HOST','localhost');
define('DB_NAME','JTL_besserpflanzen');
define('DB_USER','BP_DIETER');
define('DB_PASS','2m7tlV@9');

define('BLOWFISH_KEY', '04fa942e7adbebc229b8804d0d1026');

define('EVO_COMPATIBILITY', false);

//enables printing of warnings/infos/errors for the shop frontend
define('SHOP_LOG_LEVEL', E_ALL);
//enables printing of warnings/infos/errors for the dbeS sync
define('SYNC_LOG_LEVEL', E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
//enables printing of warnings/infos/errors for the admin backend
define('ADMIN_LOG_LEVEL', E_ALL);
//enables printing of warnings/infos/errors for the smarty templates
define('SMARTY_LOG_LEVEL', E_ALL);
//excplicitly show/hide errors
ini_set('display_errors', 0);
