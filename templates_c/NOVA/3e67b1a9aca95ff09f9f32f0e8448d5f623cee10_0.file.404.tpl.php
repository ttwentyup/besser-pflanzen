<?php
/* Smarty version 3.1.39, created on 2021-12-01 14:01:38
  from '/var/www/vhosts/20up.io/gve/templates/NOVA/page/404.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a7723293ede0_46568164',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e67b1a9aca95ff09f9f32f0e8448d5f623cee10' => 
    array (
      0 => '/var/www/vhosts/20up.io/gve/templates/NOVA/page/404.tpl',
      1 => 1638362459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:page/sitemap.tpl' => 1,
  ),
),false)) {
function content_61a7723293ede0_46568164 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_207529342261a7723293d292_25367228', 'page-404');
?>

<?php }
/* {block 'page-404-include-sitemap'} */
class Block_181231336361a7723293da54_68224272 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:page/sitemap.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'page-404-include-sitemap'} */
/* {block 'page-404'} */
class Block_207529342261a7723293d292_25367228 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page-404' => 
  array (
    0 => 'Block_207529342261a7723293d292_25367228',
  ),
  'page-404-include-sitemap' => 
  array (
    0 => 'Block_181231336361a7723293da54_68224272',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div id="page-not-found">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_181231336361a7723293da54_68224272', 'page-404-include-sitemap', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'page-404'} */
}
