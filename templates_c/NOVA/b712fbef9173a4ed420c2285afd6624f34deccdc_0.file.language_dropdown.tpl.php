<?php
/* Smarty version 3.1.39, created on 2021-12-01 14:01:36
  from '/var/www/vhosts/20up.io/gve/templates/NOVA/snippets/language_dropdown.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a77230b09548_02437790',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b712fbef9173a4ed420c2285afd6624f34deccdc' => 
    array (
      0 => '/var/www/vhosts/20up.io/gve/templates/NOVA/snippets/language_dropdown.tpl',
      1 => 1638362459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a77230b09548_02437790 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_32218610161a77230aff289_24676115', 'snippets-language-dropdown');
?>

<?php }
/* {block 'snippets-language-dropdown-text'} */
class Block_70017316661a77230b02b96_16173903 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'snippets-language-dropdown-text'} */
/* {block 'snippets-language-dropdown-item'} */
class Block_75314053361a77230b06379_63533040 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php $_block_plugin37 = isset($_smarty_tpl->smarty->registered_plugins['block']['dropdownitem'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['dropdownitem'][0][0] : null;
if (!is_callable(array($_block_plugin37, 'render'))) {
throw new SmartyException('block tag \'dropdownitem\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('dropdownitem', array('href'=>((string)$_smarty_tpl->tpl_vars['language']->value->getUrl()),'class'=>"link-lang",'data'=>array("iso"=>$_smarty_tpl->tpl_vars['language']->value->getIso()),'rel'=>"nofollow",'active'=>($_smarty_tpl->tpl_vars['language']->value->getId() == $_SESSION['kSprache'])));
$_block_repeat=true;
echo $_block_plugin37->render(array('href'=>((string)$_smarty_tpl->tpl_vars['language']->value->getUrl()),'class'=>"link-lang",'data'=>array("iso"=>$_smarty_tpl->tpl_vars['language']->value->getIso()),'rel'=>"nofollow",'active'=>($_smarty_tpl->tpl_vars['language']->value->getId() == $_SESSION['kSprache'])), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
                        <?php echo mb_strtoupper($_smarty_tpl->tpl_vars['language']->value->getIso639(), 'utf-8');?>

                    <?php $_block_repeat=false;
echo $_block_plugin37->render(array('href'=>((string)$_smarty_tpl->tpl_vars['language']->value->getUrl()),'class'=>"link-lang",'data'=>array("iso"=>$_smarty_tpl->tpl_vars['language']->value->getIso()),'rel'=>"nofollow",'active'=>($_smarty_tpl->tpl_vars['language']->value->getId() == $_SESSION['kSprache'])), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
                <?php
}
}
/* {/block 'snippets-language-dropdown-item'} */
/* {block 'snippets-language-dropdown'} */
class Block_32218610161a77230aff289_24676115 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-language-dropdown' => 
  array (
    0 => 'Block_32218610161a77230aff289_24676115',
  ),
  'snippets-language-dropdown-text' => 
  array (
    0 => 'Block_70017316661a77230b02b96_16173903',
  ),
  'snippets-language-dropdown-item' => 
  array (
    0 => 'Block_75314053361a77230b06379_63533040',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if ((isset($_SESSION['Sprachen'])) && count($_SESSION['Sprachen']) > 1) {?>
        <?php ob_start();
echo (($tmp = $_smarty_tpl->tpl_vars['dropdownClass']->value ?? null)===null||$tmp==='' ? '' : $tmp);
$_prefixVariable14=ob_get_clean();
ob_start();
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_SESSION['Sprachen'], 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
echo "
                ";
if ($_smarty_tpl->tpl_vars['language']->value->getId() == $_SESSION['kSprache']) {
echo "
                    ";
echo "
                        ";
echo mb_strtoupper($_smarty_tpl->tpl_vars['language']->value->getIso639(), 'utf-8');
echo "
                    ";
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_70017316661a77230b02b96_16173903', 'snippets-language-dropdown-text', $this->tplIndex);
echo "
                ";
}
echo "
            ";
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
$_prefixVariable15=ob_get_clean();
$_block_plugin36 = isset($_smarty_tpl->smarty->registered_plugins['block']['navitemdropdown'][0][0]) ? $_smarty_tpl->smarty->registered_plugins['block']['navitemdropdown'][0][0] : null;
if (!is_callable(array($_block_plugin36, 'render'))) {
throw new SmartyException('block tag \'navitemdropdown\' not callable or registered');
}
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('navitemdropdown', array('class'=>"language-dropdown ".$_prefixVariable14,'right'=>true,'text'=>"
            ".$_prefixVariable15));
$_block_repeat=true;
echo $_block_plugin36->render(array('class'=>"language-dropdown ".$_prefixVariable14,'right'=>true,'text'=>"
            ".$_prefixVariable15), null, $_smarty_tpl, $_block_repeat);
while ($_block_repeat) {
ob_start();?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_SESSION['Sprachen'], 'language');
$_smarty_tpl->tpl_vars['language']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->do_else = false;
?>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_75314053361a77230b06379_63533040', 'snippets-language-dropdown-item', $this->tplIndex);
?>

            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        <?php $_block_repeat=false;
echo $_block_plugin36->render(array('class'=>"language-dropdown ".$_prefixVariable14,'right'=>true,'text'=>"
            ".$_prefixVariable15), ob_get_clean(), $_smarty_tpl, $_block_repeat);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>
    <?php }
}
}
/* {/block 'snippets-language-dropdown'} */
}
