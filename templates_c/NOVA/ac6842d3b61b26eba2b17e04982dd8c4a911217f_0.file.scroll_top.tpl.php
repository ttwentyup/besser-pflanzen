<?php
/* Smarty version 3.1.39, created on 2021-12-01 14:01:37
  from '/var/www/vhosts/20up.io/gve/templates/NOVA/snippets/scroll_top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_61a7723101c9d8_41133512',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac6842d3b61b26eba2b17e04982dd8c4a911217f' => 
    array (
      0 => '/var/www/vhosts/20up.io/gve/templates/NOVA/snippets/scroll_top.tpl',
      1 => 1638362459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_61a7723101c9d8_41133512 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_111845083561a7723101bd92_52187350', 'snippets-scroll-top');
?>

<?php }
/* {block 'snippets-scroll-top-main'} */
class Block_188639127161a7723101c1f6_95201496 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <div class="smoothscroll-top go-to-top scroll-to-top">
            <span class="scroll-top-inner">
                <i class="fas fa-2x fa-chevron-up"></i>
            </span>
        </div>
    <?php
}
}
/* {/block 'snippets-scroll-top-main'} */
/* {block 'snippets-scroll-top'} */
class Block_111845083561a7723101bd92_52187350 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'snippets-scroll-top' => 
  array (
    0 => 'Block_111845083561a7723101bd92_52187350',
  ),
  'snippets-scroll-top-main' => 
  array (
    0 => 'Block_188639127161a7723101c1f6_95201496',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_188639127161a7723101c1f6_95201496', 'snippets-scroll-top-main', $this->tplIndex);
?>

<?php
}
}
/* {/block 'snippets-scroll-top'} */
}
